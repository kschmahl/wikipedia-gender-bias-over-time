# Gender bias in word embeddings from Wikipedia over time
This project is part of a research project to evaluate gender bias in word2vec embeddings generated from wikipedia over time. 
It uses the WikiCorpus code from gensim. It executes the following steps:
- Preprocess Wikipedia dumps and obtain txt files for the corpus
- Train word2vec models
- Evaluate quality of word2vec model
- Evaluate gender associations and biases over time


To reproduce the results, install the necessary requirements in requirements.txt. 
Then you can execute the full process by filling the data/raw folder with <year>.xml.bz2 
for all the available Wikipedia dumps. Then change the years array in compute_bias_all_years.py 
to match the available years. Finally, execute all computations using:

``python full_bias_computations.py``

WARNING: this may take a long time, if you don't want to preprocess and train from the beginning, 
evaluate the existing models using:

``python full_bias_computations.py -e``

After this, the results can be found as plots in the results folder and in the logs folder. 
The trained models are in data/models and can be used for more analysis.

This code is licensed using a [Creative Commons License](http://creativecommons.org/licenses/by/4.0/). This means you 
can use, edit and distribute it as long as you give appropriate credit and indicate if changes were made.


    Created based on:
    https://github.com/ruhulsbu/StereotypicalGenderAssociationsInLanguage
    Jones, Jason & Amin, Mohammad & Kim, Jessica & Skiena, Steven. (2020).
    Stereotypical Gender Associations in Language Have Decreased Over Time. 10.15195/v7.a1.